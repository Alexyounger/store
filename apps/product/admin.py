from django.contrib import admin

from apps.product.models import Category, Order, Product


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    model = Category
    list_display = ("name",)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_select_related = ("category",)
    list_display = ("name", "category", "amount", "price")


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_select_related = ("user", "product")
    list_display = ("status", "user", "product")
    raw_id_fields = ("product", "user")
    list_filter = ("status",)
    search_fields = ("=user__email",)


class OrderInline(admin.TabularInline):
    model = Order
    extra = 0
    max_num = 0
    classes = ("collapse",)
    list_select_related = ("product",)
    readonly_fields = fields = ("product_name",)

    @staticmethod
    def product_name(order):
        return order.product.name
