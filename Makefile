clean:
	black .; isort .; flake8 .;

run:
	./manage.py runserver

db:
	./manage.py migrate

test:
	coverage run manage.py test .

coverage:
	coverage run manage.py test . -v 2 && coverage report --fail-under=99.00
