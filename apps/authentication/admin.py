from django.contrib import admin

from apps.authentication.models import User
from apps.product.admin import OrderInline


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ("email", "formatted_balance", "first_name", "last_name")
    inlines = (OrderInline,)

    @staticmethod
    def formatted_balance(user):
        return f"${user.balance}"
