# Store
Sample project of an online store. Its main components are products and their categories,
as well as orders that can be created by the user.

## Setup the project locally
```bash
# Create env
python3 -m venv venv

# Activate environment
. venv/bin/activate

# Install requirements
pip install -r requirements.txt

# Create DB
touch db.sqlite3

# Apply migrations
./manage.py migrate

# Load data (Password for fake users: 1111)
./manage.py loaddata store/fixtures/fake_db_data.json

# Run project
./manage.py runserver
```

## Setup with docker
```bash
# Build image
docker build -t=store .

# Run container
docker run --name=store -p 8000:8000 -d store
```


## Curl request examples

Create products:
- products can only be created by superuser through admin page

Register new user
```bash
curl -XPOST -H "Content-type: application/json" -d '{
"email": "test@ex.com",
"password": "Qwerty@123"
}' 'http://127.0.0.1:8000/authentication/signup/'
```

Login (save token from response to use in another requests)
```bash
curl -XPOST -H "Content-type: application/json" -d '{
"email": "test@ex.com",
"password": "Qwerty@123"
}' 'http://127.0.0.1:8000/authentication/login/'
```

Get list of products
```bash
curl -XGET -H 'Authorization: Token {{token}}' -H "Content-type: application/json" 'http://127.0.0.1:8000/product/'
```

Create order
```bash
curl -XPOST -H 'Authorization: Token {{token}}' -H "Content-type: application/json" -d '{"product": 1}' 'http://127.0.0.1:8000/order/'
```

Pay for order
```bash
curl -XPOST -H 'Authorization: Token {{token}}' -H "Content-type: application/json" 'http://127.0.0.1:8000/order/{{order_id}}/pay/'
```
